sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/m/MessageBox',
	"sap/m/MessageToast",
	"sap/ui/model/resource/ResourceModel"
], function(Controller, MessageBox, MessageToast, ResourceModel) {
	"use strict";
	return Controller.extend("nnext.iq.helloWorld.controller.View1", {
		showMessage: function() {
			// read msg from i18n model
			var sMsg = this.getView().getModel("i18n").getResourceBundle().getText("helloWorld");
			
			MessageToast.show(sMsg);
		},
		onListItemPress: function (oEvt) {
			MessageBox.alert(
				"你按下去了... 這個 item 的標題是:" + oEvt.getSource().getTitle()
			);
		}
	});
});